﻿//------------------------------------------------------------------------------
// <copyright file="ProjectSettingsControl.xaml.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace JSObjectManagmentStructure
{
    using EnvDTE;
    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.Shell;
    using Microsoft.VisualStudio.Shell.Interop;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Forms;
    using System.Xml;
    using System.IO;
    using System.Xml.Serialization;
    using System.Collections.ObjectModel;
    using System.Reflection;
    using ProcessingLogic;

    /// <summary>
    /// Interaction logic for ProjectSettingsControl.
    /// </summary>
    [ProvideAutoLoad("f1536ef8-92ec-443c-9ed7-fdadf150da82")]
    public partial class ProjectSettingsControl : System.Windows.Controls.UserControl
    {
        private IVsSolution _solution;
        private IVsSolution solution
        {
            get
            {
                var tmpSolution = (IVsSolution)Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof(IVsSolution));
                var tmpProjects = GetProjects(tmpSolution);
                if (_solution == null || tmpSolution != _solution || tmpProjects == null || !tmpProjects.Any() || tmpProjects.Count() != _projectList.Count())
                {
                    _projectList = new ObservableCollection<Project>(tmpProjects);
                    _solution = tmpSolution;
                }

                return _solution;
            }
        }



        private ObservableCollection<Project> _projectList;
        public ObservableCollection<Project> ProjectList
        {
            get
            {
                return _projectList;
            }
        }

        internal static Settings Settings
        {
            get
            {
                return ModelInfoForGneration.Settings;
            }
            set
            {
                ModelInfoForGneration.Settings = value;
            }
        }

        bool isBinding = false;

        Timer timer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectSettingsControl"/> class.
        /// </summary>
        public ProjectSettingsControl()
        {
            this.InitializeComponent();

            DisableControls();

            cboDataProject.SelectionChanged += SettingChanged;
            cboBaseClassInterface.SelectionChanged += SettingChanged;
            cboBaseClass.SelectionChanged += SettingChanged;
            cboQueryClass.SelectionChanged += SettingChanged;

            cboLogicProject.SelectionChanged += SettingChanged;
            cboLogicBaseClass.SelectionChanged += SettingChanged;

            txtLogicNamespace.TextChanged += TxtLogicNamespace_TextChanged;

            cboAPIProject.SelectionChanged += SettingChanged;
            cboBaseController.SelectionChanged += SettingChanged;

            txtControllerNamespace.TextChanged += TxtControllerNamespace_TextChanged;

            txtFrontEndModelPath.GotFocus += TxtFrontEndModelPath_GotFocus;

            rdoIsTypescript.Checked += TypescriptChanged;
            rdoIsJavacript.Checked += TypescriptChanged;

            btnGenerateModel.Click += BtnGenerateModel_Click;

            timer = new Timer();
            timer.Interval = 2500;
            timer.Start();
            timer.Tick += Timer_Tick;
        }

        private void TxtLogicNamespace_TextChanged(object sender, TextChangedEventArgs e)
        {
            Settings.LogicNamespace = txtLogicNamespace.Text;
            SaveSolutionSettings();
        }

        private void TxtControllerNamespace_TextChanged(object sender, TextChangedEventArgs e)
        {
            Settings.ControllerNamespace = txtControllerNamespace.Text;
            SaveSolutionSettings();

        }

        private void BtnGenerateModel_Click(object sender, RoutedEventArgs e)
        {
            ModelInfoForGneration.GenerateCode(Settings, solution);
        }

        bool isInternalJSChange;
        private void TypescriptChanged(object sender, RoutedEventArgs e)
        {
            if (!isInternalJSChange)
            {
                isInternalJSChange = true;

                Settings.DoGenerateTypescript = rdoIsTypescript.IsChecked.Value;
                isInternalJSChange = false;
                SaveSolutionSettings();
            }
        }

        Project selectedModelProject;
        Project selectedLogicProject
        {
            get
            {
                return ModelInfoForGneration.LogicProject;
            }
            set
            {
                ModelInfoForGneration.LogicProject = value;
            }
        }
        Project selectedAPIProject
        {
            get
            {
                return ModelInfoForGneration.ControllerProject;
            }
            set
            {
                ModelInfoForGneration.ControllerProject = value;
            }
        }
        CodeInterface selectedModelInterface;
        CodeClass selectedModelBaseClass, selectedLogicBaseClass, selectedControllerBaseClass, selectedQueryClass;

        List<CodeClass> modelClasses, apiClasses, logicClasses;
        List<CodeInterface> modelInterfaces;
        bool skipRebind;
        private void SettingChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!skipRebind)
            {
                var dmProject = ProcessProjectChanged(cboDataProject, ref selectedModelProject, ref modelClasses, ref modelInterfaces, new List<System.Windows.Controls.ComboBox>() { cboBaseClass, cboQueryClass }, cboBaseClassInterface);

                Project tmpLogicProject = null;
                var lProject = ProcessProjectChanged(cboLogicProject, ref tmpLogicProject, ref logicClasses, new List<System.Windows.Controls.ComboBox>() { cboLogicBaseClass });
                selectedLogicProject = tmpLogicProject;
                Project tmpControllerProject = null;
                var apiProject = ProcessProjectChanged(cboAPIProject, ref tmpControllerProject, ref apiClasses, new List<System.Windows.Controls.ComboBox>() { cboBaseController });
                selectedAPIProject = tmpControllerProject;

                var mInterface = ProcessCodeInterfaceChanged(cboBaseClassInterface, ref selectedModelInterface);

                var mClass = ProcessCodeClassChanged(cboBaseClass, ref selectedModelBaseClass);
                var dqClass = ProcessCodeClassChanged(cboQueryClass, ref selectedQueryClass);
                var lClass = ProcessCodeClassChanged(cboLogicBaseClass, ref selectedLogicBaseClass);
                var cClass = ProcessCodeClassChanged(cboBaseController, ref selectedControllerBaseClass);

                if (!isBinding)
                {
                    skipRebind = true;
                    Settings.DataModelProject = dmProject;
                    Settings.LogicProject = lProject;
                    Settings.WebAPIProject = apiProject;

                    Settings.DataModelInterface = mInterface;

                    Settings.DataModelClass = mClass;
                    Settings.QueryClass = dqClass;
                    Settings.LogicBaseClass = lClass;
                    Settings.ControllerBaseClass = cClass;
                }

                if (cboBaseClass.SelectedItem == null && !string.IsNullOrWhiteSpace(Settings.DataModelClass) && modelClasses != null && modelClasses.Any())
                {
                    skipRebind = true;
                    cboBaseClass.SelectedItem = modelClasses.FirstOrDefault(mc => mc.FullName == Settings.DataModelClass);
                }
                if (cboBaseClassInterface.SelectedItem == null && !string.IsNullOrWhiteSpace(Settings.DataModelInterface) && modelInterfaces != null && modelInterfaces.Any())
                {
                    skipRebind = true;
                    cboBaseClassInterface.SelectedItem = modelInterfaces.FirstOrDefault(mc => mc.FullName == Settings.DataModelInterface);
                }
                if (cboQueryClass.SelectedItem == null && !string.IsNullOrWhiteSpace(Settings.QueryClass) && modelClasses != null && modelClasses.Any())
                {
                    skipRebind = true;
                    cboQueryClass.SelectedItem = modelClasses.FirstOrDefault(mc => mc.FullName == Settings.QueryClass);
                }

                if (cboLogicBaseClass.SelectedItem == null && !string.IsNullOrWhiteSpace(Settings.LogicBaseClass) && logicClasses != null && logicClasses.Any())
                {
                    skipRebind = true;
                    cboLogicBaseClass.SelectedItem = logicClasses.FirstOrDefault(lc => lc.FullName == Settings.LogicBaseClass);
                    if (string.IsNullOrWhiteSpace(txtLogicNamespace.Text))
                    {
                        var logicClass = cboLogicBaseClass.SelectedItem as CodeClass;
                        var tmpLogicNamespace = logicClass.Namespace.Name;
                        tmpLogicNamespace = tmpLogicNamespace.Substring(0, tmpLogicNamespace.LastIndexOf('.'));
                        txtLogicNamespace.Text = tmpLogicNamespace + ".Logic";
                        txtLogicNamespace.IsEnabled = true;
                        Settings.LogicNamespace = txtLogicNamespace.Text;
                    }
                }

                if (cboBaseController.SelectedItem == null && !string.IsNullOrWhiteSpace(Settings.ControllerBaseClass) && apiClasses != null && apiClasses.Any())
                {
                    skipRebind = true;
                    cboBaseController.SelectedItem = apiClasses.FirstOrDefault(ac => ac.FullName == Settings.ControllerBaseClass);
                    if (string.IsNullOrWhiteSpace(txtControllerNamespace.Text))
                    {
                        var controllerClass = cboBaseController.SelectedItem as CodeClass;
                        var tmpControllerNamespace = controllerClass.Namespace.Name;
                        tmpControllerNamespace = tmpControllerNamespace.Substring(0, tmpControllerNamespace.LastIndexOf('.'));
                        txtControllerNamespace.Text = tmpControllerNamespace + ".Controllers";
                        txtControllerNamespace.IsEnabled = true;
                        Settings.ControllerNamespace = txtControllerNamespace.Text;
                    }
                }

                SaveSolutionSettings();
                skipRebind = false;
            }
        }

        private void TxtFrontEndModelPath_GotFocus(object sender, RoutedEventArgs e)
        {
            using (FolderBrowserDialog dialog = new FolderBrowserDialog())
            {
                string solutionDirectory, slnFile, userOptions;

                solution.GetSolutionInfo(out solutionDirectory, out slnFile, out userOptions);
                dialog.SelectedPath = solutionDirectory;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    txtFrontEndModelPath.Text = dialog.SelectedPath;
                    Settings.FrontEndDirectory = txtFrontEndModelPath.Text;
                    SaveSolutionSettings();
                }
            }
            if (!string.IsNullOrWhiteSpace(Settings.FrontEndDirectory))
            {
                rdoIsJavacript.IsEnabled = true;
                rdoIsTypescript.IsEnabled = true;
            }
        }

        private string ProcessCodeInterfaceChanged(System.Windows.Controls.ComboBox cbo, ref CodeInterface selectedInterface)
        {
            if (cbo.SelectedItem != selectedInterface)
            {
                selectedInterface = cbo.SelectedItem as CodeInterface;
                if (selectedInterface != null)
                {

                }
            }
            try
            {
                return selectedInterface != null ? selectedInterface.FullName : string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        private string ProcessCodeClassChanged(System.Windows.Controls.ComboBox cbo, ref CodeClass selectedClass)
        {
            if (cbo.SelectedItem != selectedClass)
            {
                selectedClass = cbo.SelectedItem as CodeClass;
                if (selectedClass != null)
                {

                }
            }
            try
            {
                return selectedClass != null ? selectedClass.FullName : string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        private string ProcessProjectChanged(System.Windows.Controls.ComboBox cbo, ref Project selectedProject, ref List<CodeClass> classes, List<System.Windows.Controls.ComboBox> classComboList = null)
        {
            List<CodeInterface> tmpInterfaces = new List<CodeInterface>();
            string retVal = ProcessProjectChanged(cbo, ref selectedProject, ref classes, ref tmpInterfaces, classComboList, null);
            tmpInterfaces = null;
            return retVal;
        }

        private string ProcessProjectChanged(System.Windows.Controls.ComboBox cbo, ref Project selectedProject, ref List<CodeClass> classes, ref List<CodeInterface> interfaces, List<System.Windows.Controls.ComboBox> classComboList = null, System.Windows.Controls.ComboBox interfaceCombo = null)
        {

            if (cbo.SelectedItem != selectedProject)
            {
                selectedProject = cbo.SelectedItem as Project;
                bool processWithProject = false;

                try
                {
                    processWithProject = selectedProject != null && selectedProject.CodeModel != null && selectedProject.CodeModel.CodeElements != null;
                }
                catch
                {
                    processWithProject = false;
                }
                if (processWithProject)
                {
                    CodeModel model = selectedProject.CodeModel;
                    CodeElements elements = model.CodeElements;
                    if (classComboList != null && classComboList.Any())
                    {
                        classes = GetAllCodeElementsOfType(elements, vsCMElement.vsCMElementClass, false).Select(ele => (CodeClass)ele).ToList();
                        foreach (var classCombo in classComboList)
                        {
                            classCombo.DisplayMemberPath = "Name";
                            classCombo.ItemsSource = new ObservableCollection<CodeClass>(classes).OrderBy(i => i.Name);
                            classCombo.IsEnabled = classes.Any();
                        }
                    }
                    if (interfaceCombo != null)
                    {
                        interfaces = GetAllCodeElementsOfType(elements, vsCMElement.vsCMElementInterface, false).Select(ele => (CodeInterface)ele).ToList();
                        interfaceCombo.DisplayMemberPath = "Name";
                        interfaceCombo.ItemsSource = new ObservableCollection<CodeInterface>(interfaces).OrderBy(i => i.Name);
                        interfaceCombo.IsEnabled = interfaces.Any();
                    }
                }
                else
                {
                    if (interfaceCombo != null)
                    {
                        interfaceCombo.IsEnabled = false;
                        interfaceCombo.SelectedItem = null;
                        interfaceCombo.ItemsSource = null;
                    }
                    if (classComboList != null)
                    {
                        foreach (var classCombo in classComboList)
                        {
                            classCombo.IsEnabled = false;
                            classCombo.SelectedItem = null;
                            classCombo.ItemsSource = null;
                        }
                    }
                }
            }
            try
            {
                return selectedProject == null ? string.Empty : selectedProject.Name;
            }
            catch
            {
                return string.Empty;
            }
        }

        public IEnumerable<CodeElement> GetAllCodeElementsOfType(CodeElements elements, vsCMElement elementType, bool includeExternalTypes)
        {
            var ret = new List<CodeElement>();

            foreach (EnvDTE.CodeElement elem in elements)
            {
                // iterate all namespaces (even if they are external)
                // > they might contain project code
                if (elem.Kind == vsCMElement.vsCMElementNamespace)
                {
                    ret.AddRange(GetAllCodeElementsOfType(((CodeNamespace)elem).Members, elementType, includeExternalTypes));
                }
                // if its not a namespace but external
                // > ignore it
                else if (elem.InfoLocation == vsCMInfoLocation.vsCMInfoLocationExternal
                        && !includeExternalTypes)
                    continue;
                // if its from the project
                // > check its members
                else if (elem.IsCodeType)
                {
                    ret.AddRange(GetAllCodeElementsOfType(((CodeType)elem).Members, elementType, includeExternalTypes));
                }

                // if this item is of the desired type
                // > store it
                if (elem.Kind == elementType)
                    ret.Add(elem);
            }

            return ret;
        }
        private void SaveSolutionSettings()
        {
            if (isBinding) return;

            string solutionDirectory, slnFile, userOptions;

            solution.GetSolutionInfo(out solutionDirectory, out slnFile, out userOptions);
            if (!string.IsNullOrWhiteSpace(solutionDirectory))
            {
                var fileName = $"{solutionDirectory}\\js.obj.management.xml";
                var serializer = new XmlSerializer(typeof(Settings));
                using (TextWriter writer = new StreamWriter(fileName))
                {
                    serializer.Serialize(writer, Settings);
                }
            }
        }
        private void GetSolutionSettings()
        {
            string solutionDirectory, slnFile, userOptions;

            solution.GetSolutionInfo(out solutionDirectory, out slnFile, out userOptions);
            ModelInfoForGneration.SolutionDirectory = solutionDirectory;
            var fileName = $"{solutionDirectory}\\js.obj.management.xml";
            if (File.Exists(fileName))
            {
                isBinding = true;
                var serializer = new XmlSerializer(typeof(Settings));
                Settings = serializer.Deserialize(File.OpenRead(fileName)) as Settings;
            }
        }



        private void Timer_Tick(object sender, EventArgs e)
        {
            if (solution != null && ProjectList != null && ProjectList.Any())
            {
                txtFrontEndModelPath.IsEnabled = true;
                cboDataProject.IsEnabled = true;
                cboLogicProject.IsEnabled = true;
                cboAPIProject.IsEnabled = true;
                modelClasses = modelClasses ?? new List<CodeClass>();
                modelInterfaces = modelInterfaces ?? new List<CodeInterface>();
                logicClasses = logicClasses ?? new List<CodeClass>();
                apiClasses = apiClasses ?? new List<CodeClass>();
                if (cboDataProject.ItemsSource != ProjectList)
                {
                    cboDataProject.ItemsSource = ProjectList;
                    cboDataProject.DisplayMemberPath = "Name";
                    cboLogicProject.ItemsSource = ProjectList;
                    cboLogicProject.DisplayMemberPath = "Name";
                    cboAPIProject.ItemsSource = ProjectList;
                    cboAPIProject.DisplayMemberPath = "Name";
                    GetSolutionSettings();
                    if (isBinding)
                    {
                        if (!string.IsNullOrWhiteSpace(Settings.DataModelProject))
                        {
                            cboDataProject.SelectedItem = ProjectList.FirstOrDefault(p => p.Name == Settings.DataModelProject);
                        }
                        if (!string.IsNullOrWhiteSpace(Settings.LogicProject))
                        {
                            cboLogicProject.SelectedItem = ProjectList.FirstOrDefault(p => p.Name == Settings.LogicProject);
                        }
                        if (!string.IsNullOrWhiteSpace(Settings.WebAPIProject))
                        {
                            cboAPIProject.SelectedItem = ProjectList.FirstOrDefault(p => p.Name == Settings.WebAPIProject);
                        }
                        txtFrontEndModelPath.Text = Settings.FrontEndDirectory;
                        if (!string.IsNullOrWhiteSpace(Settings.FrontEndDirectory))
                        {
                            rdoIsTypescript.IsEnabled = rdoIsJavacript.IsEnabled = true;
                            rdoIsJavacript.IsChecked = !Settings.DoGenerateTypescript;
                            rdoIsTypescript.IsChecked = Settings.DoGenerateTypescript;
                        }
                        isBinding = false;
                    }
                }
            }
            else
            {
                DisableControls();
            }

            if (cboDataProject.SelectedItem != null &&
            cboBaseClass.SelectedItem != null &&
            cboBaseClassInterface.SelectedValue != null &&
            cboLogicProject.SelectedItem != null &&
            cboAPIProject.SelectedValue != null &&
            !string.IsNullOrWhiteSpace(txtFrontEndModelPath.Text))
            {
                btnGenerateModel.IsEnabled = true;
            }
            else
            {
                btnGenerateModel.IsEnabled = false;
            }

            if (cboDataProject.SelectedItem != null)
            {
                ModelInfoForGneration.ModelProject = cboDataProject.SelectedItem as Project;
            }

            ModelInfoForGneration.ModelCodeClassList = modelClasses;

            selectedModelInterface = selectedModelInterface ?? cboBaseClassInterface.SelectedItem as CodeInterface;
            ModelInfoForGneration.ModelInterface = selectedModelInterface;

            selectedModelBaseClass = selectedModelBaseClass ?? cboBaseClass.SelectedItem as CodeClass;
            ModelInfoForGneration.ModelClass = selectedModelBaseClass;
            selectedQueryClass = selectedQueryClass ?? cboQueryClass.SelectedItem as CodeClass;
            ModelInfoForGneration.QueryClass = selectedQueryClass;
            selectedLogicBaseClass = selectedLogicBaseClass ?? cboLogicBaseClass.SelectedItem as CodeClass;
            ModelInfoForGneration.LogicClass = selectedLogicBaseClass;
            selectedControllerBaseClass = selectedControllerBaseClass ?? cboBaseController.SelectedItem as CodeClass;
            ModelInfoForGneration.ControllerClass = selectedControllerBaseClass;

            Settings.ControllerNamespace = txtControllerNamespace.Text;
            Settings.LogicNamespace = txtLogicNamespace.Text;
        }

        private void DisableControls()
        {
            modelClasses = new List<CodeClass>();
            modelInterfaces = new List<CodeInterface>();
            logicClasses = new List<CodeClass>();
            apiClasses = new List<CodeClass>();
            cboDataProject.IsEnabled = false;
            cboDataProject.SelectedItem = null;
            cboBaseClass.IsEnabled = false;
            cboBaseClass.SelectedItem = null;
            cboBaseClassInterface.IsEnabled = false;
            cboBaseClassInterface.SelectedValue = null;
            cboQueryClass.IsEnabled = false;
            cboQueryClass.SelectedItem = null;
            cboLogicProject.IsEnabled = false;
            cboLogicProject.SelectedItem = null;
            cboLogicBaseClass.IsEnabled = false;
            cboLogicBaseClass.SelectedItem = null;
            txtLogicNamespace.IsEnabled = false;
            txtLogicNamespace.Text = string.Empty;
            cboAPIProject.IsEnabled = false;
            cboAPIProject.SelectedValue = null;
            cboBaseController.IsEnabled = false;
            cboBaseController.SelectedItem = null;
            txtControllerNamespace.IsEnabled = false;
            txtControllerNamespace.Text = string.Empty;
            txtFrontEndModelPath.IsEnabled = false;
            txtFrontEndModelPath.Text = string.Empty;
            rdoIsTypescript.IsEnabled = rdoIsJavacript.IsEnabled = true;
            rdoIsTypescript.IsChecked = rdoIsJavacript.IsChecked = false;
        }

        private static IEnumerable<EnvDTE.Project> GetProjects(IVsSolution solution)
        {
            foreach (IVsHierarchy hier in GetProjectsInSolution(solution))
            {
                EnvDTE.Project project = GetDTEProject(hier);
                if (project != null)
                    yield return project;
            }
        }

        private static IEnumerable<IVsHierarchy> GetProjectsInSolution(IVsSolution solution)
        {
            return GetProjectsInSolution(solution, __VSENUMPROJFLAGS.EPF_LOADEDINSOLUTION);
        }

        private static IEnumerable<IVsHierarchy> GetProjectsInSolution(IVsSolution solution, __VSENUMPROJFLAGS flags)
        {
            if (solution == null)
                yield break;

            IEnumHierarchies enumHierarchies;
            Guid guid = Guid.Empty;
            solution.GetProjectEnum((uint)flags, ref guid, out enumHierarchies);
            if (enumHierarchies == null)
                yield break;

            IVsHierarchy[] hierarchy = new IVsHierarchy[1];
            uint fetched;
            while (enumHierarchies.Next(1, hierarchy, out fetched) == VSConstants.S_OK && fetched == 1)
            {
                if (hierarchy.Length > 0 && hierarchy[0] != null)
                    yield return hierarchy[0];
            }
        }

        private static EnvDTE.Project GetDTEProject(IVsHierarchy hierarchy)
        {
            if (hierarchy == null)
                throw new ArgumentNullException("hierarchy");

            object obj;
            hierarchy.GetProperty(VSConstants.VSITEMID_ROOT, (int)__VSHPROPID.VSHPROPID_ExtObject, out obj);
            return obj as EnvDTE.Project;
        }
    }
}