﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JSObjectManagmentStructure.ProcessingLogic
{
    public class FETemplate<TIBaseEntity, TBaseEntity> : BaseClassTemplate<TIBaseEntity, TBaseEntity>
    {
        
        public FETemplate(Type _entityType, string _destinationPath) : base(_entityType, _destinationPath)
        {
        }

        internal void GenerateClass(StringBuilder modelBuilder)
        {
            StringBuilder classPropertyBuilder = new StringBuilder();
            StringBuilder classCrudBuilder = new StringBuilder();
            StringBuilder objectHydrationBuilder = new StringBuilder();
            string lowercaseEntityName = EntityType.Name;
            lowercaseEntityName = lowercaseEntityName.Substring(0, 1).ToLower() + lowercaseEntityName.Substring(1);

            foreach (var property in Properties)
            {
                GeneratePropertyInfo(property, classPropertyBuilder, classCrudBuilder, objectHydrationBuilder, lowercaseEntityName);
            }
            foreach (var property in NavigationProperties)
            {
                GeneratePropertyInfo(property, classPropertyBuilder, classCrudBuilder, objectHydrationBuilder, lowercaseEntityName);
            }
            foreach (var property in CollectionProperties)
            {
                GeneratePropertyInfo(property, classPropertyBuilder, classCrudBuilder, objectHydrationBuilder, lowercaseEntityName);
            }

            string template = ModelInfoForGneration.Settings.DoGenerateTypescript ? TS_CLASS_TEMPLATE : JS_CLASS_TEMPLATE;

            var body = classPropertyBuilder.ToString() + classCrudBuilder.ToString();

            modelBuilder.AppendLine(string.Format(template, lowercaseEntityName, EntityType.Name, body, objectHydrationBuilder.ToString()));
        }

        private void GeneratePropertyInfo(PropertyInfo property, StringBuilder classPropertyBuilder, StringBuilder classCrudBuilder, StringBuilder objectHydrationBuilder, string lowercaseEntityName)
        {
            var lowercasePropertyName = property.Name;
            lowercasePropertyName = lowercasePropertyName.Substring(0, 1).ToLower() + lowercasePropertyName.Substring(1);

            string propertyType = "any";

            classPropertyBuilder.AppendLine(string.Format((ModelInfoForGneration.Settings.DoGenerateTypescript ? TS_MEMBER_TEMPLATE : JS_MEMBER_TEMPLATE), lowercaseEntityName, lowercasePropertyName, propertyType));

            if (!property.Name.ToLower().EndsWith("id") && property.PropertyType != typeof(long))
            {
                classCrudBuilder.AppendLine(string.Format((ModelInfoForGneration.Settings.DoGenerateTypescript ? TS_CRUD_TEMPLATE : JS_CRUD_TEMPLATE), lowercasePropertyName, property.Name, propertyType, "'/" + Pluralizor.Pluralize(EntityName) + "/'+this._id+'/" + property.Name + "'"));
            }
        }


        internal override void AppendBodyParts(StringBuilder bodyBuilder, List<PropertyInfo> properties)
        {
            throw new NotImplementedException();
        }



        internal const string JS_CLASS_TEMPLATE =
    @"

Object.defineProperty(window.Entity, '{0}', {{
    get: function() {{
        return function() {{
            return function {0}Constructor(item) {{
                var {0}Self = {{}};
                
{2}
                
                return {0}Self;
            }};
        }};
    }}
}});

Object.defineProperty(window.Entity, '{1}', {{
    get: function() {{
        return window.Entity.{0};
    }}
}});";

        internal const string TS_CLASS_TEMPLATE =
    @"

    export class {0} extends BaseEntity {{

        {2}
        
        public get typeName() : String {{
            return '{1}';
        }}        

        constructor(id?:Number);
        constructor(idOrObj:Object){{
            //super constructor first, deferral/promise instantiated
            super();
            if(idOrObj) {{
                if(typeof idOrObj === 'number') {{
                    this.getById(idOrObj);                    
                }}
            }} else{{
                this.id = getNewId();
                this.entityState = EntityState.Added;                
            }}            
        }}

        private getById(id:Number) : void {{
            _$http.get().then(function(response){{
                this.construct{1}FromSource(response.data);                
            }}, this.loadDeferral.reject);
        }}

        public static $get(id:Number) : {1} {{
            return new {1}(id);
        }}

        private construct{1}FromSource(source{1}:Object) {{

            {3}

            this.loadDeferral.resolve(this);
        }}
    }}
    export class {1} extends {0} {{
        
    }}";
        internal const string TS_MEMBER_TEMPLATE = @"
        private _{1} : {2};
        public get {1}() : {2} {{
            return this._{1};
        }}
        public set {1}(value) {{
            handlePropertyValueChanged(this, this._{1}, value);
            this._{1} = value;
        }}

";

        internal const string JS_MEMBER_TEMPLATE = @"
        Object.defineProperty({0}Self, '{1}', {{
            get: function get{1}() {{
                return this._{1}; //DataType: {2}
            }}, set: function set{1}({1}Value) {{
                this._{1} = {1}Value;
            }}            
        }});

";

        internal const string TS_LOAD_ITEM_TEMPLATE = @"
            if(source{0}) {{
                this.{1} = source{0};
            }}
";

        internal const string TS_CRUD_TEMPLATE = @"
        public get{1}() : {2} {{
            var deferral = $q['defer']();
            _$http['get'](BaseEntity.apiRoot+{3}).then(function(response){{
                this.{0} = response.data;
                deferral.resolve(this.{0});
            }});            
            return deferral.promise;
        }}

        public post{1}() : {2} {{
            var deferral = $q['defer']();
            _$http['post'](BaseEntity.apiRoot+{3}, this.{0}).then(function(response){{
                this.{0} = response.data;
                deferral.resolve(this.{0});
            }});
            return deferral.promise;
        }}

        ";
        internal const string JS_CRUD_TEMPLATE = @" 
        {lowercaseEntityName}Self.get{property.Name} = function(optionalLinqQuery, optionalLinqParamObjArray) {{

        }};

        {lowercaseEntityName}Self.post{property.Name} = function() {{

        }};";
    }
}
