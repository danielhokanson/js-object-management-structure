﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JSObjectManagmentStructure.ProcessingLogic
{
    
    public class FEGenerator<TIBaseEntity, TBaseEntity> : BaseGenerator
    {
        
        public FEGenerator(Type BaseEntityType, string destinationPath) : base(BaseEntityType, destinationPath)
        {
                    
        }

        public void GenerateTemplate()
        {
            
            StringBuilder modelBodyBuilder = new StringBuilder();
            foreach(var entity in BaseGenerator.EntityList.OrderBy(e=>e.Name))
            {
                FETemplate<TIBaseEntity, TBaseEntity> template = new FETemplate<TIBaseEntity, TBaseEntity>(entity, DestinationPath);
                template.GenerateClass(modelBodyBuilder);
            }
            string modelTemplate = string.Format((ModelInfoForGneration.Settings.DoGenerateTypescript ? TS_ENTITY_TEMPLATE : JS_ENTITY_TEMPLATE), modelBodyBuilder.ToString());
            File.WriteAllText(DestinationPath + "\\Model.CodeGenerated." + (ModelInfoForGneration.Settings.DoGenerateTypescript ? "ts" : "js"), modelTemplate);
        }
        internal const string TS_ENTITY_TEMPLATE =
@"module Entity {{
    var _newId = -1;

    var getNewId = ():Number => {{
        var retVal = _newId;
        _newId++;
        return retVal;
    }};

    var handlePropertyValueChanged = (entity:BaseEntity, oldValue:any, newValue:any) : void => {{
        if(((!oldValue && newValue) || (oldValue && !newValue) ||(oldValue !== newValue)) && entity.entityState === EntityState.Unchanged){{
            entity.entityState = EntityState.Modified; 
        }}
    }}

    var _$q: any;
    export var $q = {{
        get: () => {{ return _$q; }},
        set: (value) => {{ _$q = _$q || value; }}
    }}
    var _$http: any;
    export var $http = {{
        get: () => {{ return _$http; }},
        set: (value) => {{ _$http = _$http || value; }}
    }}

    export enum EntityState {{
        //Detached = 1, <-- not used by the front end
        Unchanged = 2,
        Added = 4,
        Deleted = 8,
        Modified = 16
    }}
    
    export interface IBaseEntity{{
        id: Number;
        entityState: EntityState;
        typeName: String;                                            
    }}

    export abstract class BaseEntity implements IBaseEntity{{
        protected static apiRoot = 'api/';
        protected _$promise:any;
        protected loadDeferral:any;
        public get $promise() : any {{
            return this._$promise;
        }}
        protected _id: Number;
        public get id(): Number {{
            return this._id;
        }}
        public set id(idValue) {{
            this._id = idValue;
        }}

        private _entityState: EntityState;
        public get entityState() : EntityState{{
            return this._entityState;
        }} 
        public set entityState(entityStateValue:EntityState) {{
            this._entityState = entityStateValue;
        }}

        public abstract typeName: String;

        constructor(){{
            this.loadDeferral = _$q.defer();
            this._$promise = this.loadDeferral.promise;
        }} 
    }}

    {0}
}}
";


        internal const string JS_ENTITY_TEMPLATE =
@"Object.defineProperty(window, 'Entity', 
{{ 
    get: function() {{
        this._Entity = this._Entity || {{}};
        return this._Entity;
    }}
}});

Object.defineProperty(window.Entity, 'handlePropertyValueChanged', 
{{ 
    get: function() {{
        return function(entity, propertyName, value){{
            var oldValue = entity[propertyName];
            if(oldValue !== value){{
                entity.entityState = entity.entityState || 16; //modified if not already something other than 0
            }}
        }}
    }}
}});

Object.defineProperty(window.Entity, '$http', {{
    get: function() {{
        return this._$http;
    }},
    set: function(value) {{
        this._$http = value;
    }}
}});

Object.defineProperty(window.Entity, '$q', {{
    get: function() {{
        return this._$q;
    }},
    set: function(value) {{
        this._$q = value;
    }}
}});

{0}";
    }
}
