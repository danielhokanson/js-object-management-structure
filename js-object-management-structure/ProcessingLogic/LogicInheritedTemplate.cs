﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JSObjectManagmentStructure.ProcessingLogic
{
    internal class LogicInheritedTemplate<IBaseEntity, BaseEntity> : BaseClassTemplate<IBaseEntity, BaseEntity>
    {
        public LogicInheritedTemplate(Type _entityType, string destinationPath) : base(_entityType, destinationPath)
        {
            ClassName = $"{EntityName}Logic";
            OptionalBaseClass = $"Base{EntityName}Logic<{EntityName}>";
            if (ModelInfoForGneration.LogicClass.Namespace.Name != ModelInfoForGneration.Settings.LogicNamespace)
            {
                AddNamespace(ModelInfoForGneration.LogicClass.Namespace.Name);
            }
            AddNamespace(EntityType.Namespace);
            Namespace = ModelInfoForGneration.Settings.LogicNamespace;
            IsAbstract = EntityType.IsAbstract;
            OptionalClassComment = "CODE GENERATED: Logic is contained in the inhertied base class so the paired partial class can override the default logic";
        }

        internal override void AppendBodyParts(StringBuilder bodyBuilder, List<PropertyInfo> properties)
        {
            
        }
    }
}
