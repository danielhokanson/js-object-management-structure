﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace JSObjectManagmentStructure.ProcessingLogic
{
    public abstract class BaseGenerator
    {
        internal static List<Type> EntityList { get; set; }
        internal string DestinationPath { get; set; }
        public BaseGenerator(Type BaseEntityType, string destinationPath)
        {
            DestinationPath = destinationPath;
            if (EntityList == null || !EntityList.Any())
            {
                EntityList = EntityList ?? new List<Type>();
                
                PopulateList(BaseEntityType);
            }
        }

        internal void PopulateList(Type BaseEntityType)
        {
            if (EntityList == null)
                throw new InvalidOperationException("The specified type list has not been instantiated.");

            if (EntityList != null && EntityList.Any())
                return;

            var mappableTypeList = ModelInfoForGneration.ModelList.Where(t => BaseEntityType.IsAssignableFrom(t) &&
                                                            t != BaseEntityType &&
                                                            !EntityList.Contains(t)).ToList();
            foreach (Type objectType in mappableTypeList)
            {
                EntityList.Add(objectType);
                Console.WriteLine(string.Format("{0} Types: Added {1}", BaseEntityType.Name, objectType.Name));

            }
        }
    }
}
