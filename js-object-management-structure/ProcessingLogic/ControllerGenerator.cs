﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSObjectManagmentStructure.ProcessingLogic
{
    public class ControllerGenerator<IBaseEntity, BaseEntity> : BaseGenerator
    {
        internal static Settings Settings { get; set; } = new Settings();
        internal List<ControllerBaseItemTemplate<IBaseEntity, BaseEntity>> TemplateBaseList { get; set; }
        internal List<ControllerInheritedTemplate<IBaseEntity, BaseEntity>> TemplateList { get; set; }
        protected static List<Type> NewControllerList { get; set; }
        public ControllerGenerator(Type BaseEntityType, string destinationPath) : base(BaseEntityType, destinationPath)
        {

        }
        public void GenerateTemplates()
        {
            TemplateList = new List<ControllerInheritedTemplate<IBaseEntity, BaseEntity>>();
            TemplateBaseList = new List<ControllerBaseItemTemplate<IBaseEntity, BaseEntity>>();
            foreach (Type entityType in BaseGenerator.EntityList)
            {
                var entityName = entityType.Name;
                var baseTemplate = new ControllerBaseItemTemplate<IBaseEntity, BaseEntity>(entityType, DestinationPath);
                TemplateBaseList.Add(baseTemplate);
                baseTemplate.PrintData(true);
                baseTemplate.WriteGeneratedTemplate();
                if (!entityType.IsAbstract)
                {
                    var template = new ControllerInheritedTemplate<IBaseEntity, BaseEntity>(entityType, DestinationPath);
                    TemplateList.Add(template);
                    template.PrintData(true);
                    template.WriteGeneratedTemplate();
                }
            }
        }
    }
}
