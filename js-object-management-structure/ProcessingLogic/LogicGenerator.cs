﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSObjectManagmentStructure.ProcessingLogic
{
    public class LogicGenerator<IBaseEntity, BaseEntity> : BaseGenerator
    {
        internal List<LogicBaseItemTemplate<IBaseEntity, BaseEntity>> TemplateBaseList { get; set; }
        internal List<LogicInheritedTemplate<IBaseEntity, BaseEntity>> TemplateInheritedList { get; set; }
        public LogicGenerator(Type BaseEntityType, string destinationPath) : base(BaseEntityType, destinationPath)
        {
            
        }
        public void GenerateTemplates()
        {
            TemplateBaseList = new List<LogicBaseItemTemplate<IBaseEntity, BaseEntity>>();
            TemplateInheritedList = new List<LogicInheritedTemplate<IBaseEntity, BaseEntity>>();
            foreach (Type entityType in BaseGenerator.EntityList)
            {
                var baseTemplate = new LogicBaseItemTemplate<IBaseEntity, BaseEntity>(entityType, DestinationPath);
                TemplateBaseList.Add(baseTemplate);
                baseTemplate.PrintData(true);
                baseTemplate.WriteGeneratedTemplate();
                if (!entityType.IsAbstract)
                {
                    var inheritedTemplate = new LogicInheritedTemplate<IBaseEntity, BaseEntity>(entityType, DestinationPath);
                    TemplateInheritedList.Add(inheritedTemplate);
                    inheritedTemplate.PrintData(true);
                    inheritedTemplate.WriteGeneratedTemplate();
                }
            }
        }
    }
}
