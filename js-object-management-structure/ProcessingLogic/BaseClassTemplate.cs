﻿using EnvDTE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JSObjectManagmentStructure.ProcessingLogic
{
    public abstract class BaseClassTemplate<IBaseEntity, BaseEntity>
    {
        internal static PluralizationService Pluralizor = PluralizationService.CreateService(CultureInfo.GetCultureInfo("en-us"));
        internal Type EntityType { get; set; }
        internal string DestinationPath { get; set; }
        private string _entityName;
        internal string EntityName
        {
            get
            {
                if (EntityType != null && string.IsNullOrWhiteSpace(_entityName))
                {
                    _entityName = EntityType.Name;
                }
                return _entityName;
            }
        }
        internal string CustomClassPath { get; set; }
        internal CodeClass BaseClassType { get; set; }

        private List<PropertyInfo> _allProperties;
        internal List<PropertyInfo> AllProperties
        {
            get
            {
                if (EntityType != null && _allProperties == null)
                {
                    PropertyInfo[] tmpProperties = null;
                    if (!EntityType.BaseType.IsAbstract)
                    {
                        tmpProperties = EntityType.GetProperties(BindingFlags.DeclaredOnly |
                                    BindingFlags.Public |
                                    BindingFlags.Instance);
                    }
                    tmpProperties = tmpProperties ?? EntityType.GetProperties();
                    _allProperties = tmpProperties.Where(p => p.GetCustomAttribute<NotMappedAttribute>() == null && p.GetCustomAttribute<JsonIgnoreAttribute>() == null).OrderBy(p => p.Name).ToList();
                }
                return _allProperties;
            }
        }

        private List<PropertyInfo> _properties;
        internal List<PropertyInfo> Properties
        {
            get
            {
                if (AllProperties != null && _properties == null)
                {
                    _properties = AllProperties.Where(p => !typeof(IEnumerable<IBaseEntity>).IsAssignableFrom(p.PropertyType) &&
                                                            !typeof(IBaseEntity).IsAssignableFrom(p.PropertyType) &&
                                                            typeof(BaseEntity) != EntityType &&
                                                            !(p.PropertyType == typeof(long) &&
                                                            p.Name.ToLower().Equals("id"))
                                                            ).ToList();
                    foreach(PropertyInfo prop in _properties)
                    {
                        AddNamespace(prop.PropertyType.Namespace);
                    }
                }
                return _properties;
            }
        }
        private List<PropertyInfo> _navigationProperties;
        internal List<PropertyInfo> NavigationProperties
        {
            get
            {
                if (AllProperties != null && _navigationProperties == null)
                {
                    _navigationProperties = AllProperties.Where(p => typeof(IBaseEntity).IsAssignableFrom(p.PropertyType)).ToList();
                    foreach (PropertyInfo prop in _navigationProperties)
                    {
                        AddNamespace(prop.PropertyType.Namespace);
                    }
                }
                return _navigationProperties;
            }
        }
        private List<PropertyInfo> _collectionProperties;
        internal List<PropertyInfo> CollectionProperties
        {
            get
            {
                if (AllProperties != null && _collectionProperties == null)
                {
                    _collectionProperties = AllProperties.Where(p => typeof(IEnumerable<IBaseEntity>).IsAssignableFrom(p.PropertyType)).ToList();
                    foreach (PropertyInfo prop in _collectionProperties)
                    {
                        AddNamespace(prop.PropertyType.GetGenericArguments()[0].Namespace);
                    }
                }
                return _collectionProperties;
            }
        }
        protected List<string> NamespaceList { get; set; } = new List<string>();
        protected string Namespace { get; set; }        
        protected string ClassName { get; set; }
        protected bool IsAbstract { get; set; }
        protected string OptionalClassComment { get; set; }
        protected string OptionalBaseClass { get; set; }
        protected string OptionalGenericFilter { get; set; }
        protected string OptionalClassAttribute { get; set; }
        protected string OptionalClassMembers { get; set; }
        protected string OptionalConstructor { get; set; }

        public BaseClassTemplate(Type _entityType, string _destinationPath)
        {
            EntityType = _entityType;
            DestinationPath = _destinationPath;
            AddNamespace("System");
            AddNamespace("System.Collections.Generic");
            AddNamespace("System.Threading.Tasks");
        }

        internal void AddNamespace(string ns)
        {
            if (!NamespaceList.Any(nsli => nsli == ns))
            {
                NamespaceList.Add(ns);
            }
        }

        internal void WriteGeneratedTemplate()
        {
            StringBuilder usingStatementBuilder = new StringBuilder();

            foreach(string ns in NamespaceList.Distinct().OrderBy(ns=>ns))
            {
                usingStatementBuilder.AppendLine($"using {ns};");
            }
            List<string> addedDALNamespaces = new List<string>() { EntityType.Namespace };

            StringBuilder codeFileBuilder = new StringBuilder();
            codeFileBuilder.AppendLine(usingStatementBuilder.ToString());
            codeFileBuilder.AppendLine($"namespace {Namespace} {{");
            if (!string.IsNullOrWhiteSpace(OptionalClassComment))
            {
                if (!OptionalClassComment.StartsWith("//") || !OptionalClassComment.StartsWith("/**"))
                {
                    OptionalClassComment = $"/***{OptionalClassComment}***/";
                }
                codeFileBuilder.AppendLine($"   {OptionalClassComment}");
            }
            if (!string.IsNullOrWhiteSpace(OptionalClassAttribute))
            {
                codeFileBuilder.AppendLine($"   [{OptionalClassAttribute}]");
            }
            string abstractDeclaration = IsAbstract ? "abstract" : string.Empty;
            string classLine = $"public {abstractDeclaration} partial class {ClassName}";
            if (!string.IsNullOrWhiteSpace(OptionalBaseClass))
            {
                classLine = $"{classLine} : {OptionalBaseClass}";
            }
            if (!string.IsNullOrWhiteSpace(OptionalGenericFilter))
            {
                classLine = $@"{classLine}
        {OptionalGenericFilter}";
            }
            codeFileBuilder.AppendLine($"   {classLine} {{");
            if (!string.IsNullOrWhiteSpace(OptionalClassMembers))
            {
                codeFileBuilder.AppendLine(OptionalClassMembers);
            }
            if (!string.IsNullOrWhiteSpace(OptionalConstructor))
            {
                codeFileBuilder.AppendLine(OptionalConstructor);
            }
            codeFileBuilder.AppendLine(GenerateClassBody());
            codeFileBuilder.AppendLine("   }");
            codeFileBuilder.AppendLine("}");

            
            if (!Directory.Exists(DestinationPath))
            {
                Directory.CreateDirectory(DestinationPath);
            }
            ClassName = ClassName.Replace(" ", string.Empty);
            ClassName = ClassName.Replace("<TEntity>", string.Empty);
            ClassName = ClassName.Replace("<TEntity,TLogic>", string.Empty);
            File.WriteAllText($@"{DestinationPath}\{ClassName}.cs", codeFileBuilder.ToString());
        }

        private string GenerateClassBody()
        {
            StringBuilder bodyBuilder = new StringBuilder();
            AppendBodyParts(bodyBuilder, Properties);
            AppendBodyParts(bodyBuilder, NavigationProperties);
            AppendBodyParts(bodyBuilder, CollectionProperties);
            return bodyBuilder.ToString();
        }

        internal void PrintData(bool provideDetail = false)
        {
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("==============================================");
            Console.WriteLine(string.Format("Name: {0}", EntityName));
            Console.WriteLine("----------------------------------------------");
            PrintPropertyListData("All Properties", AllProperties, false);
            PrintPropertyListData("Properties", Properties, provideDetail);
            PrintPropertyListData("Navigation Properties", NavigationProperties, provideDetail);
            PrintPropertyListData("Collection Properties", CollectionProperties, provideDetail);
        }
        private void PrintPropertyListData(string alias, List<PropertyInfo> properties, bool provideDetail)
        {
            Console.WriteLine("----------------------------------------------");
            Console.WriteLine(string.Format("{0}: {1}", alias, properties.Count()));
            if (provideDetail)
            {
                Console.WriteLine("______________________________________________");
                foreach (PropertyInfo prop in properties)
                {
                    Console.WriteLine(string.Format("{0}: {1}", prop.Name, prop.PropertyType.Name));
                }
            }
            Console.WriteLine("----------------------------------------------");
        }
        internal abstract void AppendBodyParts(StringBuilder bodyBuilder, List<PropertyInfo> properties);
    }
}
