﻿using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using EnvDTE;
using System.Reflection;
using System.IO;
using System.Security.Policy;
using System.Windows.Forms;

namespace JSObjectManagmentStructure.ProcessingLogic
{
    internal static class ModelInfoForGneration
    {
        internal static Settings Settings { get; set; } = new Settings();
        internal static string SolutionDirectory { get; set; }
        internal static string ModelBinaryPath { get; set; }
        internal static string ControllerBinaryPath { get; set; }
        internal static Assembly ModelAssembly { get; set; }
        internal static Assembly ControllerAssembly { get; set; }
        internal static List<Type> ModelList { get; set; }
        internal static List<CodeClass> ModelCodeClassList { get; set; }
        internal static EnvDTE.Project ModelProject { get; set; }
        internal static EnvDTE.Project LogicProject { get; set; }
        internal static EnvDTE.Project ControllerProject { get; set; }
        internal static string FEPath { get; set; }
        internal static CodeClass ModelClass { get; set; }
        internal static CodeClass QueryClass { get; set; }
        internal static CodeInterface ModelInterface { get; set; }
        internal static CodeClass LogicClass { get; set; }
        public static CodeClass ControllerClass { get; internal set; }

        internal static void GenerateCode(Settings settings, IVsSolution solution)
        {

            Settings = settings;
            var slnDir = new DirectoryInfo(SolutionDirectory);
            var binDirectories = slnDir.GetDirectories("Bin", SearchOption.AllDirectories);
            Action<DirectoryInfo> deleteFiles = null;
            deleteFiles = (DirectoryInfo directory) =>
            {
                foreach (DirectoryInfo childDir in directory.GetDirectories())
                {
                    deleteFiles(childDir);
                }
                foreach (FileInfo file in directory.GetFiles())
                {
                    try
                    {
                        file.Delete();
                    }
                    catch
                    {
                        //should be okay if some of these fail, some of these are likely to be the temp copies created to prevent build locking
                    }
                }
            };
            foreach (var bin in binDirectories)
            {
                deleteFiles(bin);
            }
            ProjectSettingsPackage.DTE.Solution.SolutionBuild.Clean(true);
            ProjectSettingsPackage.DTE.Solution.SolutionBuild.BuildProject("Debug", ModelProject.FullName, true);
            string binDirectoryPath = string.Empty;
            DirectoryInfo binDirInfo = null;
            if (string.IsNullOrWhiteSpace(ModelBinaryPath))
            {
                binDirectoryPath = ModelProject.FullName.Replace($"{ModelProject.Name}.csproj", "Bin");
                binDirInfo = new DirectoryInfo(binDirectoryPath);
                var binFile = binDirInfo.GetFiles($"{ModelProject.Name}.dll", SearchOption.AllDirectories).OrderByDescending(f => f.LastWriteTime).FirstOrDefault();

                ModelInfoForGneration.ModelBinaryPath = binFile != null ? binFile.FullName : string.Empty;
            }
            //if (string.IsNullOrWhiteSpace(ControllerBinaryPath))
            //{
            //    var binDirectory = ControllerProject.FullName.Replace($"{ControllerProject.Name}.csproj", "Bin");
            //    DirectoryInfo binDir = new DirectoryInfo(binDirectory);
            //    var binFile = binDir.GetFiles($"{ControllerProject.Name}.dll", SearchOption.AllDirectories).OrderByDescending(f => f.LastWriteTime).FirstOrDefault();

            //    ModelCodeGenerator.ControllerBinaryPath = binFile != null ? binFile.FullName : string.Empty;
            //}

            if (string.IsNullOrWhiteSpace(ModelBinaryPath))
            {
                return;
            }

            

            Guid copiedDLLUIdentifier = Guid.NewGuid();

            string newFileName = ModelBinaryPath.Replace(".dll", $"{copiedDLLUIdentifier.ToString()}.dll");

            File.Copy(ModelBinaryPath, newFileName);

            ModelAssembly = Assembly.LoadFrom(newFileName);
            try
            {

                Type baseModel = ModelAssembly.GetType(Settings.DataModelClass);
                Type baseInterfaceModel = ModelAssembly.GetType(Settings.DataModelInterface);

                ModelList = ModelCodeClassList.Select(mc => ModelAssembly.GetType(mc.FullName)).ToList();

                var genericTypeArguments = new Type[] { baseInterfaceModel, baseModel };

                var logicGeneratorType = typeof(LogicGenerator<,>).MakeGenericType(genericTypeArguments);
                var controllerGeneratorType = typeof(ControllerGenerator<,>).MakeGenericType(genericTypeArguments);
                var feTemplateType = typeof(FEGenerator<,>).MakeGenericType(genericTypeArguments);

                var logicGeneratorConstructor = logicGeneratorType.GetConstructor(new Type[] { typeof(Type), typeof(string) });
                var logicGenerator = logicGeneratorConstructor.Invoke(new object[] { baseModel, LogicProject.FullName.Replace($"{LogicProject.Name}.csproj", "Logic_Generated") });
                var logicTemplateGeneration = logicGenerator.GetType().GetMethod("GenerateTemplates");
                logicTemplateGeneration.Invoke(logicGenerator, null);

                var controllerGeneratorConstructor = controllerGeneratorType.GetConstructor(new Type[] { typeof(Type), typeof(string) });
                var controllerGenerator = controllerGeneratorConstructor.Invoke(new object[] { baseModel, ControllerProject.FullName.Replace($"{ControllerProject.Name}.csproj", "Controllers_Generated") });
                var controllerTemplateGeneration = controllerGenerator.GetType().GetMethod("GenerateTemplates");
                controllerTemplateGeneration.Invoke(controllerGenerator, null);



                var feTemplateConstructor = feTemplateType.GetConstructor(new Type[] { typeof(Type), typeof(string) });
                var feTemplate = feTemplateConstructor.Invoke(new object[] { baseModel, ModelInfoForGneration.Settings.FrontEndDirectory });
                var feTemplateGeneration = feTemplate.GetType().GetMethod("GenerateTemplate");
                feTemplateGeneration.Invoke(feTemplate, null);
                MessageBox.Show("Code Generation Completed Successfully");
            }
            catch (Exception e)
            {
                MessageBox.Show("Code Generation Failed:" + e.Message);
            }
           
        }
    }
}
