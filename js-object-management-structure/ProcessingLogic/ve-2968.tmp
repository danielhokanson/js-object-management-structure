﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace JSObjectManagmentStructure.ProcessingLogic
{
    internal class ControllerBaseItemTemplate<IBaseEntity, BaseEntity> : BaseClassTemplate<IBaseEntity, BaseEntity>
    {
        internal Type BaseControllerType { get; set; }


        internal ControllerBaseItemTemplate(Type entityType, string destinationPath) : base(entityType, destinationPath)
        {
            ClassName = $"Base{Pluralizor.Pluralize(entityType.Name)}Controller<TEntity, TLogic>";
            IsAbstract = true;
            if (entityType.BaseType.IsAbstract)
            {
                OptionalBaseClass = $"BaseEntityController<TEntity, TLogic>";
            }
            else
            {
                OptionalBaseClass = $"Base{Pluralizor.Pluralize(entityType.BaseType.Name)}Controller<TEntity, TLogic>";
            }

            if (CodeGenerator.ControllerClass.Namespace.Name != CodeGenerator.Settings.ControllerNamespace)
            {
                AddNamespace(CodeGenerator.ControllerClass.Namespace.Name);
            }
            if (!string.IsNullOrWhiteSpace(CodeGenerator.Settings.QueryClass))
            {
                AddNamespace(CodeGenerator.QueryClass.Namespace.Name);
            }
            AddNamespace(CodeGenerator.Settings.LogicNamespace);
            AddNamespace(EntityType.Namespace);
            AddNamespace("System.Web.Http");
            AddNamespace("System.Web.Http.Description");
            Namespace = CodeGenerator.Settings.ControllerNamespace;
            OptionalGenericFilter = $@"where TEntity : {entityType.Name}, new()
        where TLogic : Base{entityType.Name}Logic<TEntity>, new()";
        }
        internal override void AppendBodyParts(StringBuilder bodyBuilder, List<PropertyInfo> properties)
        {
            if (properties == null || !properties.Any())
                return;

            var template = @"

        /// <summary>
        /// AUTO GENERATED
        /// Gets the {1} property of {2} by the primary key
        /// </summary>
        /// <param name=""id"">The Primary Key of the {2} object {1} will be selected by</param>
        [Route(""{{id}}/{1}""),HttpGet(), ResponseType(typeof({3}))]
        public virtual async Task<IHttpActionResult> Get{1}(long id) {{
            try 
            {{
                var retVal = await logic.Get{1}Async(id);
                return Ok(retVal);
            }}
            catch (Exception e)
            {{
                Log.Error(e.Message, e);
                return InternalServerError(e);
            }}
        }}
" + (string.IsNullOrWhiteSpace(CodeGenerator.Settings.QueryClass)?"": @"
        /// <summary>
        /// AUTO GENERATED
        /// Gets the {1} property of {2} by the primary key, allowing an additional dynamic linq expression to be posted
        /// </summary>
        /// <param name=""id"">The Primary Key of the {2} object {1} will be selected by</param>
        /// <param name=""queryData"">The query information that will be used to limit data in addition to the {0}'s Id</param>
        [Route(""{{id}}/{1}Query""),HttpPost(), ResponseType(typeof({3}))]
        public virtual async Task<IHttpActionResult> Get{1}ByQuery(long id, "+ CodeGenerator.Settings.QueryClass + @" queryData) {{
            try 
            {{
                var retVal = await logic.Get{1}Async(id, queryData);
                return Ok(retVal);
            }}
            catch (Exception e)
            {{
                Log.Error(e.Message, e);
                return InternalServerError(e);
            }}
        }}")+@"

        /// <summary>
        /// AUTO GENERATED
        /// Updates the {1} property of {2}
        /// </summary>
        /// <param name=""id"">The Primary Key of the {2} object {1} will be updated on</param>
        /// <param name=""{4}"">The value that will be set to the {1}'s {2} will be set to</param>
        [Route(""{{id}}/{1}""), HttpPost(), ResponseType(typeof({3}))]
        public virtual async Task<IHttpActionResult> Update{1}(long id, [FromBody] {3} {4}) {{
            try 
            {{
                var retVal = await logic.Update{1}Async(id, {4});
                return Ok(retVal);
            }}
            catch (Exception e)
            {{
                Log.Error(e.Message, e);
                throw;
            }}
        }}
";

            foreach (PropertyInfo property in properties)
            {
                var methodVariableName = property.Name.Substring(0, 1).ToLower() + property.Name.Substring(1);
                var methodVariableType = "";
                var distinct = string.Empty;
                var propertyType = property.PropertyType;
                var generics = propertyType.GetGenericArguments();
                if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    methodVariableType = generics[0].Name + "?";
                }
                else if (propertyType.IsGenericType)
                {
                    methodVariableType = "List<" + generics[0].Name + ">";
                    distinct = ".Distinct()";
                }
                else
                {
                    methodVariableType = propertyType.Name;
                }


                bodyBuilder.Append(string.Format(template, Pluralizor.Pluralize(EntityType.Name), property.Name, EntityType.Name, methodVariableType, methodVariableName, distinct));
            }
        }
    }
}
