﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JSObjectManagmentStructure.ProcessingLogic
{
    internal class LogicBaseItemTemplate<IBaseEntity, BaseEntity> : BaseClassTemplate<IBaseEntity, BaseEntity>
    {        
        public LogicBaseItemTemplate(Type entityType, string destinationPath) : base(entityType, destinationPath)
        {
            ClassName = $"Base{entityType.Name}Logic<TEntity>";

          
            AddNamespace("System.Data.Entity");
            AddNamespace("System.Linq");
            AddNamespace("System.Linq.Dynamic");
            AddNamespace(ModelInfoForGneration.LogicClass.Namespace.Name);

            if (entityType.BaseType.IsAbstract)
            {
                OptionalBaseClass = $"{ModelInfoForGneration.LogicClass.Name}<TEntity>";
            }
            else
            {
                OptionalBaseClass = $"Base{entityType.BaseType.Name}Logic<TEntity>";
            }
            if (ModelInfoForGneration.LogicClass.Namespace.Name != ModelInfoForGneration.Settings.LogicNamespace)
            {
                AddNamespace(ModelInfoForGneration.LogicClass.Namespace.Name);
            }
            if (!string.IsNullOrWhiteSpace(ModelInfoForGneration.Settings.QueryClass))
            {
                AddNamespace(ModelInfoForGneration.QueryClass.Namespace.Name);
            }
            AddNamespace(EntityType.Namespace);
            Namespace = ModelInfoForGneration.Settings.LogicNamespace;
            OptionalGenericFilter = $@"where TEntity : {entityType.Name}, new()";
            OptionalClassMembers = 
$@" private DbSet<{entityType.Name}> _data;
        protected DbSet<{entityType.Name}> {entityType.Name}Data
        {{
            get
            {{
                if (_data == null)
                {{
                    _data = db.Set<{entityType.Name}>();
                }}
                return _data;
            }}
        }}";
            IsAbstract = true;
        }

        internal override void AppendBodyParts(StringBuilder bodyBuilder, List<PropertyInfo> properties)
        {
            if (properties == null || !properties.Any())
                return;

            var template = @"

        /// <summary>
        /// AUTO GENERATED
        /// Gets the {0} property of {1} by the primary key
        /// </summary>
        /// <param name=""id"">The Primary Key of the {1} object {0} will be selected by</param>
        public virtual {2} Get{0}(long id) {{
            try 
            {{
                var retVal = (from t in {1}Data
                                where t.Id==id
                                select t.{0}{4}).FirstOrDefault();
                return retVal;
            }}
            catch (Exception e)
            {{
                throw;
            }}
        }}

        /// <summary>
        /// AUTO GENERATED
        /// Gets the {0} property of {1} by the primary key asynchronously
        /// </summary>
        /// <param name=""id"">The Primary Key of the {1} object {0} will be selected by</param>
        public virtual async Task<{2}> Get{0}Async(long id) {{
            try 
            {{
                var retVal = await (from t in {1}Data
                                where t.Id==id
                                select t.{0}{4}).FirstOrDefaultAsync();
                return  retVal;
            }}
            catch (Exception e)
            {{
                throw;
            }}
        }}
" + (string.IsNullOrWhiteSpace(ModelInfoForGneration.Settings.QueryClass) ? "" : @"
        /// <summary>
        /// AUTO GENERATED
        /// Gets the {0} property of {1} by the primary key, allowing an additional dynamic linq expression to be posted
        /// </summary>
        /// <param name=""id"">The Primary Key of the {1} object {0} will be selected by</param>
        /// <param name=""queryData"">The query information that will be used to limit data in addition to the {1}'s Id</param>
        public virtual {2} Get{0}(long id, " + ModelInfoForGneration.Settings.QueryClass + @" queryData) {{
            try 
            {{
                var retVal =  (from t in {1}Data.Where(queryData.Query, queryData.Parameters)
                                where t.Id==id
                                select t.{0}{4}).FirstOrDefault();
                return retVal;
            }}
            catch (Exception e)
            {{
                throw;
            }}
        }}

        /// <summary>
        /// AUTO GENERATED
        /// Gets the {0} property of {1} by the primary key, allowing an additional dynamic linq expression to be posted asynchronously
        /// </summary>
        /// <param name=""id"">The Primary Key of the {1} object {0} will be selected by</param>
        /// <param name=""queryData"">The query information that will be used to limit data in addition to the {1}'s Id</param>
        public virtual async Task<{2}> Get{0}Async(long id, " + ModelInfoForGneration.Settings.QueryClass + @" queryData) {{
            try 
            {{
                var retVal =  await (from t in {1}Data.Where(queryData.Query, queryData.Parameters)
                                where t.Id==id
                                select t.{0}{4}).FirstOrDefaultAsync();
                return retVal;
            }}
            catch (Exception e)
            {{
                throw;
            }}
        }}
") +@"
        /// <summary>
        /// AUTO GENERATED
        /// Updates the {0} property of {1}
        /// </summary>
        /// <param name=""id"">The Primary Key of the {1} object {0} will be updated on</param>
        /// <param name=""{3}"">The value that will be set to the {1}'s {0}</param>
        public virtual {2} Update{0}(long id, {2} {3}) {{
            try 
            {{
                var retVal =  {1}Data.Find(id);
                retVal.{0} = {3};
                db.Entry(retVal).Property(p => p.{0}).IsModified = true;
                db.SaveChanges();
                return retVal.{0};
            }}
            catch (Exception e)
            {{
                throw;
            }}
        }}

        /// <summary>
        /// AUTO GENERATED
        /// Updates the {0} property of {1} asynchronously
        /// </summary>
        /// <param name=""id"">The Primary Key of the {1} object {0} will be updated on</param>
        /// <param name=""{3}"">The value that will be set to the {1}'s {0}</param>
        public virtual async Task<{2}> Update{0}Async(long id, {2} {3}) {{
             try 
            {{
                var retVal =  await {1}Data.FindAsync(id);
                retVal.{0} = {3};
                db.Entry(retVal).Property(p => p.{0}).IsModified = true;
                await db.SaveChangesAsync();
                return retVal.{0};
            }}
            catch (Exception e)
            {{
                throw;
            }}
        }}
";

            foreach (PropertyInfo property in properties)
            {
                var methodVariableName = property.Name.Substring(0, 1).ToLower() + property.Name.Substring(1);
                var methodVariableType = "";
                var distinct = string.Empty;
                var propertyType = property.PropertyType;
                var generics = propertyType.GetGenericArguments();
                if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    methodVariableType = generics[0].Name + "?";
                }
                else if (propertyType.IsGenericType)
                {
                    methodVariableType = "List<" + generics[0].Name + ">";
                    distinct = ".Distinct().ToList()";
                }
                else
                {
                    methodVariableType = propertyType.Name;
                }


                bodyBuilder.Append(string.Format(template, property.Name, EntityType.Name, methodVariableType, methodVariableName, distinct));
            }
        }
    }
}
