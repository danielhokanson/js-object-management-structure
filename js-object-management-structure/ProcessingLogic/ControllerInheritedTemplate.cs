﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace JSObjectManagmentStructure.ProcessingLogic
{
    internal class ControllerInheritedTemplate<IBaseEntity, BaseEntity> : BaseClassTemplate<IBaseEntity, BaseEntity>
    {



        internal ControllerInheritedTemplate(Type entityType, string destinationPath) : base(entityType, destinationPath)
        {
            ClassName = Pluralizor.Pluralize(entityType.Name) + "Controller";
            AddNamespace("System.Web.Http");
            if (ModelInfoForGneration.ControllerClass.Namespace.Name != ModelInfoForGneration.Settings.ControllerNamespace)
            {
                AddNamespace(ModelInfoForGneration.ControllerClass.Namespace.Name);
            }
            AddNamespace(ModelInfoForGneration.Settings.LogicNamespace);
            AddNamespace(EntityType.Namespace);
            AddNamespace("System.Web.Http");
            AddNamespace("System.Web.Http.Description");
            Namespace = ModelInfoForGneration.Settings.ControllerNamespace;
            OptionalBaseClass = $"Base{Pluralizor.Pluralize(entityType.Name)}Controller<{EntityName}, {EntityName}Logic>";
            OptionalClassAttribute = $"RoutePrefix(\"api/{Pluralizor.Pluralize(entityType.Name)}\")";
        }


        internal override void AppendBodyParts(StringBuilder bodyBuilder, List<PropertyInfo> properties)
        {
        }
    }
}
