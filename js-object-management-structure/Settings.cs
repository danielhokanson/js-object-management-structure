﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSObjectManagmentStructure
{
    public class Settings
    {
        public string DataModelProject { get; set; }
        public string DataModelInterface { get; set; }
        public string DataModelClass { get; set; }
        public string LogicProject { get; set; }
        public string LogicBaseClass { get; set; }
        public string LogicNamespace { get; set; }
        public string WebAPIProject { get; set; }
        public string ControllerBaseClass { get; set; }
        public string ControllerNamespace { get; set; }
        public string FrontEndDirectory { get; set; }
        public bool DoGenerateTypescript { get; set; } = false;
        public string QueryClass { get; set; }
    }
}
